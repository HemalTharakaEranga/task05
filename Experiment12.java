package experiments;

import java.util.*;
import java.util.function.Function;

import cricket.Club;

/******************************************************************************
 * This version does the same as the last except it is more explicit about the
 * Function implementation.
 * 
 * @author Dr Kevan Buckley, University of Wolverhampton, 2019
 ******************************************************************************/

public class Experiment12 {
  class ERemover implements Function<String, String> {
    public String apply(String name) {
      return name.replaceAll("[eE]", "");
    }
  }
  
  public void run() {
	     List<Club> table = Arrays.asList(
	             new Club(1, "Sri Lanka", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
	             new Club(2, "India", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
	             new Club(3, "England", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
	             new Club(4, "Australia", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
	             new Club(5, "Pakistan", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
	             new Club(6, "Bangladesh", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
	             new Club(7, "Afghanistan", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
	             new Club(8, "South Africa", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
	             new Club(9, "Zimbabwe", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
	             new Club(10, "UAE", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
	             new Club(11, "Oman", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
	             new Club(12, "Nepal", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
	     );

	        table.stream()
            .map(Club::getcricket_teams)
            .map(new ERemover())
            .forEach(name -> System.out.println(name));

  }

  public static void main(String[] args) {
    new Experiment12().run();
  }
}
