package experiments;

import java.util.*;

import java.util.function.Consumer;

import cricket.Club;
/******************************************************************************
 * This version is similar to the last but the interface implementation is
 * done more explicity. To do this the custom consumer has to implement the 
 * accept method.
 * 
 * @author Dr Kevan Buckley, University of Wolverhampton, 2019
 ******************************************************************************/

public class Experiment06 {
    class ClubPrintConsumer implements Consumer<Club> {
        public void accept(Club club) {
            System.out.println(club.getcricket_teams());
        }
  }
  
  public void run() {
      List<Club> table = Arrays.asList(
              new Club(1, "Sri Lanka", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
              new Club(2, "India", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
              new Club(3, "England", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
              new Club(4, "Australia", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
              new Club(5, "Pakistan", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
              new Club(6, "Bangladesh", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
              new Club(7, "Afghanistan", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
              new Club(8, "South Africa", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
              new Club(9, "Zimbabwe", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
              new Club(10, "UAE", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
              new Club(11, "Oman", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
              new Club(12, "Nepal", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
      );

      table.forEach(new ClubPrintConsumer());
  }
  
  public static void main(String[] args) {
    new Experiment06().run();
  }
}
