package experiments;

import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.function.IntUnaryOperator;
import java.util.stream.IntStream;

import cricket.Club;

/******************************************************************************
 * Another example of implementing a functional interface. This is included
 * because it shows a function that returns a different type to its input.
 * 
 * author Dr Kevan Buckley, University of Wolverhampton, 2019
 ******************************************************************************/

public class Experiment15 {
  public static void main(String[] args) {
    List<Club> table = Arrays.asList(
        new Club(1, "Sri Lanka", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
        new Club(2, "India", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
        new Club(3, "England", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
        new Club(4, "Australia", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
        new Club(5, "Pakistan", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
        new Club(6, "Bangladesh", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
        new Club(7, "Afghanistan", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
        new Club(8, "South Africa", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
        new Club(9, "Zimbabwe", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
        new Club(10, "UAE", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
        new Club(11, "Oman", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
        new Club(12, "Nepal", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
    );

    Function<Integer, Double> reciprocal = new Function<Integer, Double>() {
      public Double apply(Integer n) {
        return 1.0 / n;
      }
    };
    System.out.println(reciprocal.apply(10));

    IntStream denominators = IntStream.range(1, 10);
    denominators.mapToDouble(reciprocal::apply)
        .forEach(System.out::println);
  }
}
