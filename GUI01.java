package gui;

import java.awt.*;
import java.awt.event.*;
import java.util.Arrays;
import java.util.List;

import javax.swing.*;

import cricket.Club;

public class GUI01 {

  public static void main(String[] args) {
    List<Club> table = Arrays.asList(
        new Club(1, "Sri Lanka", 23, 10, 5, 8, 620, 530, 231, 78, 49, 89, 20, 77),
        new Club(2, "India", 23, 16, 0, 6, 625, 414, 211, 72, 43, 9, 2, 75),
        new Club(3, "England", 23, 15, 1, 6, 453, 421, 32, 37, 39, 4, 2, 68),
        new Club(4, "Australia", 23, 14, 1, 7, 664, 418, 246, 70, 40, 5, 5, 68),
        new Club(5, "Pakistan", 23, 14, 0, 8, 663, 437, 226, 70, 46, 5, 7, 68),
        new Club(6, "Bangladesh", 23, 10, 2, 9, 672, 527, 145, 77, 56, 9, 4, 61),
        new Club(7, "Afghanistan", 23, 12, 0, 11, 497, 482, 15, 62, 54, 6, 4, 54),
        new Club(8, "South Africa", 23, 10, 0, 12, 444, 514, -70, 45, 50, 4, 5, 49),
        new Club(9, "Zimbabwe", 23, 9, 15, 12, 553, 575, -22, 53, 61, 4, 6, 48),
        new Club(10, "UAE", 23, 7, 1, 14, 443, 578, -136, 46, 57, 4, 6, 40),
        new Club(11, "Oman", 23, 5, 1, 16, 475, 543, -70, 57, 61, 4, 8, 73),
        new Club(12, "Nepal", 23, 0, 0, 22, 223, 1025, -799, 29, 147, 1, 0, 73)
    );

    JFrame f = new JFrame();
    JButton b = new JButton("Press Me");
    f.setLayout(new BorderLayout());
    f.add(b, BorderLayout.CENTER);

    b.addActionListener(event -> {
      for (Club club : table) {
        System.out.println(club.toString());
      }
    });

    f.setSize(400, 300); 
    f.setResizable(false);
    f.setLocationRelativeTo(null);
    f.setVisible(true);
  }
}