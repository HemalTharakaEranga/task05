package experiments;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.function.Supplier;

import cricket.Club;

/******************************************************************************
 * This is another example of using a supplier. Suppliers will be used like 
 * this in realtime applications for capturing things like sensor readings.
 * 
 * author Dr Kevan Buckley, University of Wolverhampton, 2019
 ******************************************************************************/

public class Experiment16 {
  public static void main(String[] args) {
    List<Club> table = Arrays.asList(
        new Club(1, "Sri Lanka", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
        new Club(2, "India", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
        new Club(3, "England", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
        new Club(4, "Australia", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
        new Club(5, "Pakistan", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
        new Club(6, "Bangladesh", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
        new Club(7, "Afghanistan", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
        new Club(8, "South Africa", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
        new Club(9, "Zimbabwe", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
        new Club(10, "UAE", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
        new Club(11, "Oman", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
        new Club(12, "Nepal", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
    );

    Supplier<String> dateSupplier = () -> {
      LocalDate date = LocalDate.now(); 
      return date.toString();
    };  
    System.out.printf("Today is %s", dateSupplier.get());
  }
}

