package files;

import java.io.*;

/******************************************************************************
 * This program sorts the lines of a file so that the shortest lines are 
 * output last.
 *  
 * @author Dr Kevan Buckley, University of Wolverhampton, 2019
 ******************************************************************************/

public class Files05 {

  public static void main(String[] args) throws Exception {
    BufferedReader r = new BufferedReader(new FileReader("C:/Users/thara/Documents/CINEC_NOTE/Advanced_software_workshop/Function/functional-programming-1/data/cricket.txt"));

    r.lines().sorted((am, bn) -> {
      if (am.length() == bn.length())
        return 0;
      if (am.length() < bn.length())
        return 1;
      return -1;
    }).forEach(l -> System.out.println(l));

    r.close();
  }

}
