package experiments;
import cricket.Club;
import java.util.*;
import java.util.function.Predicate;

import cricket.Club;

/******************************************************************************
 * This version introduces uses a predicate for the same task more explicitly.
 * 
 * @author Dr Kevan Buckley, University of Wolverhampton, 2019
 ******************************************************************************/

public class Experiment10 {
  class EFilter implements Predicate<String>{
    public boolean test(String name) {
    	 return name.contains("e") || name.contains("E");
    }    
  }

  public void run() {
	     List<Club> table = Arrays.asList(
	             new Club(1, "Sri Lanka", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
	             new Club(2, "India", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
	             new Club(3, "England", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
	             new Club(4, "Australia", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
	             new Club(5, "Pakistan", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
	             new Club(6, "Bangladesh", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
	             new Club(7, "Afghanistan", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
	             new Club(8, "South Africa", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
	             new Club(9, "Zimbabwe", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
	             new Club(10, "UAE", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
	             new Club(11, "Oman", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
	             new Club(12, "Nepal", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
	     );

	        table.stream()
            .map(Club::getcricket_teams)
            .filter(new EFilter())
            .forEach(System.out::println);

  }

  public static void main(String[] args) {
    new Experiment10().run();
  }
}
