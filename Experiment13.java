package experiments;

import java.util.Arrays;
import java.util.List;
import java.util.function.Supplier;

import cricket.Club;

/******************************************************************************
 * This version uses a Supplier to generate names;
 * 
 * @author Dr Kevan Buckley, University of Wolverhampton, 2019
 ******************************************************************************/

public class Experiment13 {
  class NameSupplier implements Supplier<String>{
	     List<Club> table = Arrays.asList(
	             new Club(1, "Sri Lanka", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
	             new Club(2, "India", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
	             new Club(3, "England", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
	             new Club(4, "Australia", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
	             new Club(5, "Pakistan", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
	             new Club(6, "Bangladesh", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
	             new Club(7, "Afghanistan", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
	             new Club(8, "South Africa", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
	             new Club(9, "Zimbabwe", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
	             new Club(10, "UAE", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
	             new Club(11, "Oman", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
	             new Club(12, "Nepal", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
	     );

    int nextIndex = 0;
    public String get() {
        if (nextIndex < table.size()) {
            return table.get(nextIndex++).getcricket_teams();
        }
        return null;
    } 
  }
  
  public void run() {
    NameSupplier ns = new NameSupplier();

    System.out.println(ns.get());
    System.out.println(ns.get());
    System.out.println(ns.get());
    System.out.println(ns.get());
    System.out.println(ns.get());
    System.out.println(ns.get());
    System.out.println(ns.get());
    System.out.println(ns.get());
    System.out.println(ns.get());
    System.out.println(ns.get());
    System.out.println(ns.get());
    System.out.println(ns.get());
    System.out.println(ns.get());
  }

  public static void main(String[] args) {
    new Experiment13().run();
  }
}
